import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appQuestionBorder]'
})

export class QuestionColorDirective {

  @Input() set appShadowHover(borderClass: string) {
    if (borderClass) {
      this.borderClass = borderClass;
    }
  };

  borderClass = 'border';

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  // addClass() {
  //   this.renderer.addClass(this.el.nativeElement, this.borderClass);
  // }
  // removeClass() {
  //   this.renderer.removeClass(this.el.nativeElement, this.borderClass);
  // }

  @HostListener('mouseenter') addClass() {
    // this.el.nativeElement.classList.add('shadow');
    this.renderer.addClass(this.el.nativeElement, this.borderClass);
  }

  @HostListener('mouseleave') removeClass() {
    // this.el.nativeElement.classList.remove('shadow');
    this.renderer.removeClass(this.el.nativeElement, this.borderClass);
  }


}
