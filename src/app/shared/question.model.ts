export class Question {
  constructor(
    public question: string,
    public answer: string,
    public help: string,
    public status: string
  ) {}
}
