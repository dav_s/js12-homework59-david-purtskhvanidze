import { Question } from './question.model';

export class QuestionsService {
  private questionsList: Question[] = [
    new Question(
      'На какой свет нужно переходить дорогу',
      'зелёный',
      'Трава',
      'нет ответа'),
    new Question(
      'Вечный президент Росии',
      'путин',
      'Фамилия ~ Дорогин',
      'нет ответа'),
    new Question(
      'День рождения грустный праздник?',
      'да',
      'Да',
      'нет ответа'),
  ]

  getQuestions() {
    return this.questionsList.slice();
  }

  addStatus(question: string, answer: string) {
    const currentQuestion = this.questionsList.find(i => i.question === question)!;

    if (answer === currentQuestion.answer) {
      currentQuestion.status = 'ответ верный';
    } else {
      currentQuestion.status = 'ответ неверный';
    }
    console.log(currentQuestion);
  }

  getQuestionsCount() {
    this.getCorrectAnsweredQuestionsCount()
    return this.questionsList.length;

  }

  getCorrectAnsweredQuestionsCount() {
    const correctAnsweredQuestions = this.questionsList.find(i => i.status === 'ответ верный');



  }


}
