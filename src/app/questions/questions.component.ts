import { Component, OnInit } from '@angular/core';
import { Question } from '../shared/question.model';
import { QuestionsService } from '../shared/questions.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  questions!: Question[];
  count = this.questionService.getQuestionsCount();

  constructor(public questionService: QuestionsService) {}

  ngOnInit() {
    this.questions = this.questionService.getQuestions();
  }

}
