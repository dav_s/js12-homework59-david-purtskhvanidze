import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Question } from '../../shared/question.model';
import { QuestionsService } from '../../shared/questions.service';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent {
  @ViewChild('answerInput') answerInput!: ElementRef;
  @Input() question!: Question;

  constructor(public questionService: QuestionsService) {}

  questionSubmit(question: string) {
    const answerInput: string = this.answerInput.nativeElement.value;

    this.questionService.addStatus(question, answerInput.toLocaleLowerCase());

  }

}
